<?php

namespace Doctoreto\Log;

use Monolog\Logger;
use Illuminate\Log\LogManager;
use Monolog\Handler\PsrHandler;
use Illuminate\Support\ServiceProvider;
use Doctoreto\Log\Logger as DoctoretoLogger;
use Illuminate\Contracts\Container\Container;
use Illuminate\Http\Request;

class LogServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__.'../database/migrations');
        }
    }
    /**
     *
     * @return void
     */
    public function register()
    {
        if ($this->app['log'] instanceof LogManager) {
            $this->app['log']->extend('doctoreto-log', function (Container $app, array $config) {
                $doctoretoLogger = new DoctoretoLogger($this->app->request);
                $handler = new PsrHandler($doctoretoLogger);
                return new Logger('doctoreto-log', [$handler]);
            });
        }
    }
}
